
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    private int _playerScore;

    private int _computerScore;

    public Text playerScrText;
    public Text computerScrText;
    public Ball ball;

    public void PlayerScores()
    {
        _playerScore++;
        Debug.Log("Player");
        Debug.Log(_playerScore);
        this.playerScrText.text=_playerScore.ToString();
        this.ball.ResetPosition();
    }

    public void ComputerScores()
    {
        _computerScore++;
        Debug.Log("Computer");
        Debug.Log(_computerScore);
        this.computerScrText.text=_computerScore.ToString();
        this.ball.ResetPosition();
    }
}
